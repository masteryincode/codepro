﻿using FluentMigrator;

namespace CodePro.Migrations {
    [Migration(2)]
    public class SizesMigration : Migration {
        public override void Up() {
            Insert.IntoTable("size")
                .Row(
                    new {
                        name = "",
                        fullname = "",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "XS",
                        fullname = "EXTRA SMALL",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "S",
                        fullname = "SMALL",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "A",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "M",
                        fullname = "MEDIUM",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "B",
                        synonym_2 = "MED",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "L",
                        fullname = "LARGE",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "XL",
                        fullname = "EXTRA LARGE",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "X LARGE",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "XXL",
                        fullname = "EXTRA EXTRA LARGE",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "2XL",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "1X",
                        fullname = "1X PLUS",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "2X",
                        fullname = "2X PLUS",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "3X",
                        fullname = "3X PLUS",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "4X",
                        fullname = "4X PLUS",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "5X",
                        fullname = "5X PLUS",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "00P",
                        fullname = "00P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "0P",
                        fullname = "0P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "2P",
                        fullname = "2P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "4P",
                        fullname = "4P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "6P",
                        fullname = "6P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "8P",
                        fullname = "8P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "10P",
                        fullname = "10P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "12P",
                        fullname = "12P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "14P",
                        fullname = "14P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    })
                .Row(
                    new {
                        name = "16P",
                        fullname = "16P",
                        number = -1,
                        is_numeric = false,
                        synonym_1 = "",
                        synonym_2 = "",
                        synonym_3 = "",
                        created_timestamp = 0,
                        created_by = -1
                    });
        }

        public override void Down() {
            Delete.FromTable("size").AllRows();
        }
    }
}