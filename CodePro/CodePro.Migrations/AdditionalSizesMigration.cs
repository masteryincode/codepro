﻿using FluentMigrator;

namespace CodePro.Migrations {
    [Migration(4)]
    public class AdditionalSizesMigration : Migration {
        public override void Up() {
            Update.Table("size").Set(new {synonym_2 = "A OR SMALL"}).Where(new {name = "S"});
            Update.Table("size").Set(new {synonym_3 = "B OR MED"}).Where(new {name = "M"});
            Update.Table("size").Set(new {synonym_1 = "XS NO CUP"}).Where(new {name = "XS"});
        }

        public override void Down() {
            Update.Table("size").Set(new {synonym_2 = ""}).Where(new {name = "S"});
            Update.Table("size").Set(new {synonym_3 = ""}).Where(new {name = "M"});
            Update.Table("size").Set(new {synonym_1 = ""}).Where(new {name = "XS"});
        }
    }
}