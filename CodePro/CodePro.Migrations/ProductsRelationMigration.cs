﻿using FluentMigrator;

namespace CodePro.Migrations {
    [Migration(3)]
    public class ProductsRelationMigration : Migration {
        public override void Up() {
            Create.Table("products_relation")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("product_list_id")
                .AsInt32()
                .ForeignKey("fk_products_relation_product_list_id", "product_list", "id")
                .NotNullable()
                .WithColumn("main_product_id")
                .AsInt32()
                .ForeignKey("fk_products_relation_product_id_1", "product", "id")
                .NotNullable()
                .WithColumn("related_product_id")
                .AsInt32()
                .ForeignKey("fk_products_relation_product_id_2", "product", "id")
                .NotNullable()
                .WithColumn("sequence_order").AsInt32().NotNullable()
                .WithColumn("created_timestamp").AsInt32().NotNullable()
                .WithColumn("created_by")
                .AsInt32()
                .ForeignKey("fk_products_relation_login_id_1", "login", "id")
                .NotNullable()
                .WithColumn("deleted_timestamp").AsInt32().Nullable()
                .WithColumn("deleted_by")
                .AsInt32()
                .ForeignKey("fk_products_relation_login_id_2", "login", "id")
                .Nullable();
        }

        public override void Down() {
            Delete.Table("products_relation");
        }
    }
}