﻿using FluentMigrator;

namespace CodePro.Migrations {
    [Migration(1)]
    public class Migration1 : Migration {
        public override void Up() {
            Create.Table("login")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("name").AsString().NotNullable().Unique();

            Insert.IntoTable("login").Row(new {id = -1, name = "system"});

            Create.Table("size")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("name").AsString().NotNullable().Unique()
                .WithColumn("fullname").AsString().NotNullable().Unique()
                .WithColumn("number").AsInt32().NotNullable().Indexed("size_number_idx")
                .WithColumn("is_numeric").AsBoolean().NotNullable()
                .WithColumn("synonym_1").AsString().NotNullable().Indexed("size_s1_idx")
                .WithColumn("synonym_2").AsString().NotNullable().Indexed("size_s2_idx")
                .WithColumn("synonym_3").AsString().NotNullable().Indexed("size_s3_idx")
                .WithColumn("created_timestamp").AsInt32().NotNullable()
                .WithColumn("created_by").AsInt32().ForeignKey("fk_size_login_id_1", "login", "id").NotNullable()
                .WithColumn("deleted_timestamp").AsInt32().Nullable()
                .WithColumn("deleted_by").AsInt32().ForeignKey("fk_size_login_id_2", "login", "id").Nullable();

            Create.Table("color")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("name").AsString().NotNullable().Unique()
                .WithColumn("fullname").AsString().NotNullable().Unique()
                .WithColumn("synonym_1").AsString().NotNullable().Indexed("color_s1_idx")
                .WithColumn("synonym_2").AsString().NotNullable().Indexed("color_s2_idx")
                .WithColumn("synonym_3").AsString().NotNullable().Indexed("color_s3_idx")
                .WithColumn("created_timestamp").AsInt32().NotNullable()
                .WithColumn("created_by").AsInt32().ForeignKey("fk_color_login_id_1", "login", "id").NotNullable()
                .WithColumn("deleted_timestamp").AsInt32().Nullable()
                .WithColumn("deleted_by").AsInt32().ForeignKey("fk_color_login_id_2", "login", "id").Nullable();

            Create.Table("product")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("upc").AsInt64().NotNullable().Unique()
                .WithColumn("description").AsString().NotNullable()
                .WithColumn("vendor_or_style_number").AsString().NotNullable()
                .WithColumn("size_text").AsString().NotNullable()
                .WithColumn("size_id").AsInt32().ForeignKey("fk_product_size_id", "size", "id").NotNullable()
                .WithColumn("color_text").AsString().NotNullable()
                .WithColumn("color_id").AsInt32().ForeignKey("fk_product_color_id", "color", "id").NotNullable()
                .WithColumn("division").AsString().NotNullable()
                .WithColumn("department_name").AsString().NotNullable()
                .WithColumn("vendor_name").AsString().NotNullable()
                .WithColumn("image_url").AsString().NotNullable()
                .WithColumn("created_timestamp").AsInt32().NotNullable()
                .WithColumn("created_by").AsInt32().ForeignKey("fk_product_login_id_1", "login", "id").NotNullable()
                .WithColumn("deleted_timestamp").AsInt32().Nullable()
                .WithColumn("deleted_by").AsInt32().ForeignKey("fk_product_login_id_2", "login", "id").Nullable();

            Create.Table("product_list")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("name").AsString().NotNullable().Unique()
                .WithColumn("created_timestamp").AsInt32().NotNullable()
                .WithColumn("created_by")
                .AsInt32()
                .ForeignKey("fk_product_list_login_id_1", "login", "id")
                .NotNullable()
                .WithColumn("deleted_timestamp").AsInt32().Nullable()
                .WithColumn("deleted_by").AsInt32().ForeignKey("fk_product_list_login_id_2", "login", "id").Nullable();

            Create.Table("product_to_product_list_buffer")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("product_list_name").AsString().NotNullable()
                .WithColumn("upc").AsInt64().NotNullable().Indexed("product_to_product_list_buffer_idx")
                .WithColumn("description").AsString().NotNullable()
                .WithColumn("original_quantity").AsInt32().NotNullable()
                .WithColumn("original_cost").AsDecimal(16, 6).NotNullable()
                .WithColumn("original_retail").AsDecimal(16, 6).NotNullable()
                .WithColumn("vendor_or_style_number").AsString().NotNullable()
                .WithColumn("color").AsString().NotNullable()
                .WithColumn("size").AsString().NotNullable()
                .WithColumn("client_cost").AsDecimal(16, 6).NotNullable()
                .WithColumn("division").AsString().NotNullable()
                .WithColumn("department_name").AsString().NotNullable()
                .WithColumn("vendor_name").AsString().NotNullable()
                .WithColumn("image_url").AsString().NotNullable()
                .WithColumn("status").AsString().NotNullable().WithDefaultValue("NOT_PROCESSED")
                .WithColumn("processing_result").AsString().NotNullable().WithDefaultValue("")
                .WithColumn("created_timestamp").AsInt32().NotNullable()
                .WithColumn("created_by")
                .AsInt32()
                .ForeignKey("fk_product_to_product_list_buffer_login_id_1", "login", "id")
                .NotNullable()
                .WithColumn("deleted_timestamp").AsInt32().Nullable()
                .WithColumn("deleted_by")
                .AsInt32()
                .ForeignKey("fk_product_to_product_list_buffer_login_id_2", "login", "id")
                .Nullable();

            Create.Table("product_to_product_list")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("product_id")
                .AsInt32()
                .ForeignKey("fk_product_to_product_list_product_id", "product", "id")
                .NotNullable()
                .WithColumn("product_list_id")
                .AsInt32()
                .ForeignKey("fk_product_to_product_list_product_list_id", "product_list", "id")
                .NotNullable()
                .WithColumn("quantity").AsInt32().NotNullable()
                .WithColumn("original_cost").AsDecimal(16, 6).NotNullable()
                .WithColumn("original_retail").AsDecimal(16, 6).NotNullable()
                .WithColumn("client_cost").AsDecimal(16, 6).NotNullable()
                .WithColumn("created_timestamp").AsInt32().NotNullable()
                .WithColumn("created_by")
                .AsInt32()
                .ForeignKey("fk_product_to_product_list_login_id_1", "login", "id")
                .NotNullable()
                .WithColumn("deleted_timestamp").AsInt32().Nullable()
                .WithColumn("deleted_by")
                .AsInt32()
                .ForeignKey("fk_product_to_product_list_login_id_2", "login", "id")
                .Nullable();

            Create.Index("product_to_product_list_idx")
                .OnTable("product_to_product_list")
                .OnColumn("product_id").Unique()
                .OnColumn("product_list_id");
        }

        public override void Down() {
            Delete.Table("product_to_product_list");
            Delete.Table("product_to_product_list_buffer");
            Delete.Table("product_list");
            Delete.Table("product");
            Delete.Table("color");
            Delete.Table("size");
            Delete.Table("login");
        }
    }
}