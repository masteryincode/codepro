﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodePro.DAO;
using CodePro.Generators;
using CodePro.POCO;
using CodePro.Properties;
using NLog;

namespace CodePro.BufferProcessor {
    public class ProductToProductListBufferProcessor {
        private static readonly Logger Log = LogManager.GetLogger("BufferProcessor");
        private readonly Action<int, string> _onNext;

        public ProductToProductListBufferProcessor(Action<int, string> onNext) {
            _onNext = onNext;
        }

        public int? Process(List<ProductToProductListBuffer> buffers) {
            if (buffers.Count == 0) {
                _onNext(100, Resources.DONE);
                return null;
            }

            _onNext(0, Resources.STARTED);
            Log.Info(Resources.PRODUCT_LIST_CREATION_IS_STARTED);
            var productList = new ProductList {
                Id = -1,
                Name = buffers[0].ProductListName,
                CreatedBy = -1,
                CreatedTimestamp = buffers[0].CreatedTimestamp
            };

            var productLists = ProductListDao.GetByName(buffers[0].ProductListName);

            if (productLists.Count > 0) {
                productList = productLists[0];
            } else {
                ProductListDao.Save(productList);
            }
            Log.Info(Resources.PRODUCT_LIST_CREATION_IS_FINISHED);

            _onNext(5, Resources.PRODUCT_LIST_CREATION_IS_FINISHED);

            Log.Info(Resources.BUFFERS_MERGING_IS_STARTED);
            var removedBuffers = new List<ProductToProductListBuffer>();
            var buffersByUpc = from b in buffers where b.Id != -1 group b by b.Upc;
            var newBuffers = buffersByUpc.Select(grouping => {
                var quantity = grouping.AsEnumerable().Sum(buffer => buffer.OriginalQuantity);
                var result = grouping.First();

                result.OriginalQuantity = quantity;

                if (grouping.Count() > 1) {
                    removedBuffers.AddRange(grouping.Skip(1));
                }

                return result;
            }).ToList();

            removedBuffers.AddRange(from b in buffers where b.Id == -1 select b);

            Log.Info(Resources.BUFFERS_MERGING_IS_FINISHED, removedBuffers.Count);

            _onNext(10, string.Format(Resources.BUFFERS_MERGING_IS_FINISHED, removedBuffers.Count));

            foreach (var mergedBuffer in removedBuffers) {
                mergedBuffer.Status = Resources.PROCESSED;
                mergedBuffer.ProcessingResult = Resources.INFO_REMOVED;
            }

            Log.Info(Resources.REMOVED_BUFFERS_SAVING_IS_STARTED);
            ProductToProductListBufferDao.SaveAll(removedBuffers);
            Log.Info(Resources.REMOVED_BUFFERS_SAVING_IS_FINISHED);

            _onNext(15, Resources.REMOVED_BUFFERS_SAVING_IS_FINISHED);
            var progress = 15;
            var productsToCreate = new List<Product>();
            var productToProductListLinksToCreate =
                new List<KeyValuePair<ProductToProductListLink, ProductToProductListBuffer>>();

            Log.Info(Resources.MAIN_BUFFERS_PROCESSING_IS_STARTED);
            for (var i = 0; i < newBuffers.Count; ++i) {
                var buffer = newBuffers[i];
                var product = ProductDao.GetByUpc(buffer.Upc);
                var descriptionText = buffer.Description;
                var vendorOrStyleNumberText = buffer.VendorOrStyleNumber;
                var colorText = buffer.Color;
                var sizeText = buffer.Size;
                var divisionText = buffer.Division;
                var departmentNameText = buffer.DepartmentName;
                var vendorNameText = buffer.VendorName;
                var imageUrlText = buffer.ImageUrl;

                if (product == null) {
                    var color = ColorDao.Find(colorText);

                    if (color == null) {
                        color = new Color(colorText);
                        ColorDao.Save(color);
                    }

                    var size = SizeDao.Find(sizeText);

                    if (size == null) {
                        size = new Size(sizeText);
                        SizeDao.Save(size);
                    }

                    product = new Product {
                        Id = -1,
                        Upc = buffer.Upc,
                        Description = descriptionText,
                        VendorOrStyleNumber = vendorOrStyleNumberText,
                        ColorText = colorText,
                        Color = color,
                        SizeText = sizeText,
                        Size = size,
                        Division = divisionText,
                        DepartmentName = departmentNameText,
                        VendorName = vendorNameText,
                        ImageUrl = imageUrlText,
                        CreatedBy = -1,
                        CreatedTimestamp = productList.CreatedTimestamp
                    };

                    productsToCreate.Add(product);
                }

                var link = new ProductToProductListLink {
                    Id = -1,
                    ProductInfo = new ProductInfo {
                        Product = product,
                        OriginalCost = buffer.OriginalCost,
                        Quantity = buffer.OriginalQuantity,
                        ClientCost = buffer.ClientCost,
                        OriginalRetail = buffer.OriginalRetail
                    },
                    ProductList = productList,
                    CreatedBy = -1,
                    CreatedTimestamp = productList.CreatedTimestamp
                };

                productToProductListLinksToCreate.Add(
                    new KeyValuePair<ProductToProductListLink, ProductToProductListBuffer>(link, buffer));

                var currentProgress = 15 + (int) (i * 15.0 / newBuffers.Count);

                if (currentProgress <= progress) continue;

                _onNext(currentProgress, Resources.MAIN_BUFFERS_PROCESSING);
                progress = currentProgress;
            }

            Log.Info(Resources.MAIN_BUFFERS_PROCESSING_IS_FINISHED);

            if (progress < 30) {
                _onNext(30, Resources.MAIN_BUFFERS_PROCESSING_IS_FINISHED);
            }

            Log.Info(Resources.PRODUCTS_CREATION_IS_STARTED);
            ProductDao.Create(productsToCreate);
            Log.Info(Resources.PRODUCTS_CREATION_IS_FINISHED);

            _onNext(50, Resources.PRODUCTS_CREATION_IS_FINISHED);

            Log.Info(Resources.PRODUCT_TO_PRODUCT_LIST_LINKS_CREATION_IS_STARTED);
            if (ProductToProductListLinkDao.Create(productToProductListLinksToCreate.Select(pair => pair.Key))) {
                productToProductListLinksToCreate.ForEach(pair => {
                    pair.Value.Status = Resources.PROCESSED;
                    pair.Value.ProcessingResult = Resources.OK;
                });
            }
            Log.Info(Resources.PRODUCT_TO_PRODUCT_LIST_LINKS_CREATION_IS_FINISHED);

            _onNext(70, Resources.PRODUCT_TO_PRODUCT_LIST_LINKS_CREATION_IS_FINISHED);

            Log.Info(Resources.MAIN_BUFFERS_SAVING_IS_STARTED);
            ProductToProductListBufferDao.SaveAll(newBuffers);
            Log.Info(Resources.MAIN_BUFFERS_SAVING_IS_FINISHED);

            _onNext(85, Resources.MAIN_BUFFERS_SAVING_IS_FINISHED);

            var vendorOrStyleNumberPlusColorToProduct = new Dictionary<string, List<Product>>();

            Log.Info(Resources.PRODUCTS_RELATIONS_CREATION_IS_STARTED);
            productToProductListLinksToCreate.ForEach(pair => {
                var vendorOrStyleNumberPlusColor = pair.Key.ProductInfo.Product.VendorOrStyleNumber +
                                                   pair.Key.ProductInfo.Product.Color.Name;

                if (vendorOrStyleNumberPlusColorToProduct.ContainsKey(vendorOrStyleNumberPlusColor)) {
                    vendorOrStyleNumberPlusColorToProduct[vendorOrStyleNumberPlusColor].Add(pair.Key.ProductInfo.Product);
                } else {
                    vendorOrStyleNumberPlusColorToProduct[vendorOrStyleNumberPlusColor] = new List<Product> {
                        pair.Key.ProductInfo.Product
                    };
                }
            });

            var productsRelationsToCreate = new List<ProductsRelation>();

            vendorOrStyleNumberPlusColorToProduct.Values.ToList()
                .ForEach(
                    list => { productsRelationsToCreate.Add(ProductsRelationGenerator.Generate(productList, list)); });

            ProductsRelationDao.Create(productsRelationsToCreate);
            Log.Info(Resources.PRODUCTS_RELATIONS_CREATION_IS_FINISHED);

            _onNext(100, Resources.PRODUCTS_RELATIONS_CREATION_IS_FINISHED);

            return productList.Id;
        }
    }
}