﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CodePro.BufferProcessor;
using CodePro.DAO;
using CodePro.Export;
using CodePro.Parsers.Excel;
using CodePro.POCO;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro {
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public sealed partial class MainWindow {
        private static readonly Logger Log = LogManager.GetLogger("CodePro");

        private readonly Dictionary<ApplicationState, Action> _onStateActions;

        private readonly ObservableCollection<Scanning> _scannings = new ObservableCollection<Scanning>();

        private readonly Dictionary<ApplicationState, HashSet<ApplicationState>> _transitions = new Dictionary
            <ApplicationState, HashSet<ApplicationState>> {
                {
                    ApplicationState.Started,
                    new HashSet<ApplicationState> {ApplicationState.Started, ApplicationState.ProductLists}
                }, {
                    ApplicationState.ProductLists,
                    new HashSet<ApplicationState> {ApplicationState.ProductLists, ApplicationState.Scanning}
                }, {
                    ApplicationState.Scanning,
                    new HashSet<ApplicationState> {ApplicationState.ProductLists, ApplicationState.Scanning}
                }
            };

        private int? _currentProductListId;
        private List<ProductList> _currentProductLists;
        private Scanning _currentScanning;

        private ApplicationState _currentState = ApplicationState.Started;
        private long? _previousSku;

        public MainWindow() {
            InitializeComponent();

            using (var conn = DB.Utils.GetConnection()) {
                try {
                    conn.Open();
                } catch (MySqlException e) {
                    Log.Error(Properties.Resources.CANT_OPEN_CONNECTION + ": " + e);
                    MessageBox.Show(Properties.Resources.CANT_OPEN_CONNECTION);
                }
            }

            _onStateActions = new Dictionary<ApplicationState, Action> {
                {
                    ApplicationState.Started, () => {
                        ViewScanningItem.IsEnabled = false;
                        GenerateImgFileMenuItem.IsEnabled = false;

                        ScanningPanel.Visibility = Visibility.Collapsed;
                        ScanningDataGrid.Visibility = Visibility.Collapsed;
                        ListSelectionGrid.Visibility = Visibility.Collapsed;
                        ProductListDataGrid.Visibility = Visibility.Collapsed;

                        ProductListComboBox.DisplayMemberPath = "UserFriendlyName";
                        ProductListComboBox.SelectedValuePath = "Id";

                        CurrentProductLists = ProductListDao.Get();

                        ViewProductListsItem.IsEnabled = CurrentProductLists.Count > 0;
                    }
                }, {
                    ApplicationState.Scanning, () => {
                        ListSelectionGrid.Visibility = Visibility.Collapsed;
                        ProductListDataGrid.Visibility = Visibility.Collapsed;

                        ScanningPanel.Visibility = Visibility.Visible;
                        ScanningDataGrid.Visibility = Visibility.Visible;

                        ScanTextBox.Focus();

                        ScanningDataGrid.ItemsSource = _scannings;
                    }
                }, {
                    ApplicationState.ProductLists, () => {
                        ScanningPanel.Visibility = Visibility.Collapsed;
                        ScanningDataGrid.Visibility = Visibility.Collapsed;

                        ListSelectionGrid.Visibility = Visibility.Visible;
                        ProductListDataGrid.Visibility = Visibility.Visible;

                        CurrentProductLists = ProductListDao.Get();

                        ProductListComboBox.ItemsSource = CurrentProductLists;

                        if (CurrentProductListId != null) {
                            ProductListComboBox.SelectedValue = CurrentProductListId;

                            var products = ProductDao.GetProductsPlainInfoByProductList((int) CurrentProductListId);

                            ProductListDataGrid.ItemsSource = products;
                        }
                    }
                }
            };

            ChangeState(ApplicationState.Started);
        }

        private int? CurrentProductListId {
            get { return _currentProductListId; }
            set {
                if (_currentProductListId != null && _currentProductListId == value) return;

                _currentProductListId = value;
            }
        }

        private List<ProductList> CurrentProductLists {
            get { return _currentProductLists; }
            set {
                if (_currentProductLists != null && _currentProductLists == value) return;

                _currentProductLists = value;
            }
        }

        private long? PreviousSku {
            get { return _previousSku; }
            set {
                if (_previousSku != null && _previousSku == value) return;

                _previousSku = value;
            }
        }

        private Scanning CurrentScanning {
            get { return _currentScanning; }
            set {
                if (_currentScanning != null && _currentScanning == value) return;

                _currentScanning = value;
            }
        }

        private void ChangeState(ApplicationState newState) {
            if (!_transitions[_currentState].Contains(newState)) {
                return;
            }

            _currentState = newState;
            _onStateActions[newState].Invoke();
        }

        private void Exit_Click(object sender, RoutedEventArgs e) {
            Close();
        }

        private void ImportProductList(string fileName) {
            Task.Factory.StartNew(() => {
                var parser =
                    new ProductListParser(
                        (i => {
                            Application.Current.Dispatcher.Invoke(
                                () => {
                                    Title = $"{Application.ResourceAssembly.GetName().Name}: " +
                                            string.Format(Properties.Resources.PARSING, i);
                                });
                        }));

                return parser.Parse(fileName);
            }).ContinueWith(task => {
                var buffers = task.Result;
                var sw = new Stopwatch();
                var total = new TimeSpan();

                sw.Start();
                Log.Info(Properties.Resources.BUFFER_SAVING_IS_STARTED);
                if (ProductToProductListBufferDao.SaveAll(buffers)) {
                    Log.Info(Properties.Resources.BUFFER_SAVING_IS_FINISHED,
                        Properties.Resources.SUCCESSFULLY);
                    sw.Stop();
                    Log.Info(@"Elapsed={0}", sw.Elapsed);
                    total += sw.Elapsed;

                    var processor =
                        new ProductToProductListBufferProcessor(
                            (i, message) => {
                                Application.Current.Dispatcher.Invoke(
                                    () => {
                                        Title = $"{Application.ResourceAssembly.GetName().Name}: " +
                                                string.Format(Properties.Resources.PROCESSING, i, message);
                                    });
                            });

                    sw.Restart();
                    Log.Info(Properties.Resources.BUFFER_PROCESSING_IS_STARTED);
                    var productListId = processor.Process(buffers);
                    Log.Info(Properties.Resources.BUFFER_PROCESSING_IS_FINISHED);
                    sw.Stop();
                    Log.Info(@"Elapsed={0}", sw.Elapsed);
                    total += sw.Elapsed;
                    Log.Info(@"Total (buffers save & process) elapsed={0}", total);

                    Application.Current.Dispatcher.Invoke(() => {
                        CurrentProductLists = ProductListDao.Get();
                        ProductListComboBox.ItemsSource = CurrentProductLists;

                        if (_currentState == ApplicationState.ProductLists) {
                            if (productListId != null) {
                                CurrentProductListId = productListId;
                                ProductListComboBox.SelectedValue = CurrentProductListId;
                                ProductListDataGrid.ItemsSource =
                                    ProductDao.GetProductsPlainInfoByProductList(CurrentProductListId.Value);
                            }
                        } else {
                            Task.Factory.StartNew(() => {
                                Task.Delay(TimeSpan.FromSeconds(3));

                                Application.Current.Dispatcher.Invoke(() => {
                                    ChangeState(ApplicationState.ProductLists);

                                    if (productListId != null) {
                                        CurrentProductListId = productListId;
                                        ProductListComboBox.SelectedValue = CurrentProductListId;
                                        var products =
                                            ProductDao.GetProductsPlainInfoByProductList(CurrentProductListId.Value);

                                        ProductListDataGrid.ItemsSource = products;
                                        ViewScanningItem.IsEnabled = true;
                                        GenerateImgFileMenuItem.IsEnabled = true;

                                        _scannings.Clear();
                                    }
                                });
                            });
                        }

                        ViewProductListsItem.IsEnabled = CurrentProductLists.Count > 0;
                    });
                } else {
                    Log.Warn(Properties.Resources.BUFFER_SAVING_IS_FINISHED,
                        Properties.Resources.UNSUCCESSFULLY);
                }

                Task.Factory.StartNew(() => {
                    Task.Delay(TimeSpan.FromSeconds(5));

                    Application.Current.Dispatcher.Invoke(() => { Title = Application.ResourceAssembly.GetName().Name; });
                });
            });
        }

        private void ImportProductList_Click(object sender, RoutedEventArgs e) {
            var dialog = new OpenFileDialog {
                DefaultExt = Properties.Resources.EXCEL_DEFAULT_EXT,
                Filter = Properties.Resources.EXCEL_FILTER,
                FilterIndex = 2
            };

            if (dialog.ShowDialog() ?? false) {
                ImportProductList(dialog.FileName);
            }
        }

        private void ShowProductLists_Click(object sender, RoutedEventArgs e) {
            ChangeState(ApplicationState.ProductLists);
        }

        private void ProductListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (ProductListComboBox.SelectedValue == null) return;

            CurrentProductListId = (int) ProductListComboBox.SelectedValue;
            var products = ProductDao.GetProductsPlainInfoByProductList((int) CurrentProductListId);

            ProductListDataGrid.ItemsSource = products;
            ViewScanningItem.IsEnabled = true;
            GenerateImgFileMenuItem.IsEnabled = true;

            _scannings.Clear();
        }

        private void ViewScanningItem_Click(object sender, RoutedEventArgs e) {
            ChangeState(ApplicationState.Scanning);
        }

        private void GenerateImgFileMenuItem_Click(object sender, RoutedEventArgs e) {
            var saveFileDialog = new SaveFileDialog {
                Filter = Properties.Resources.EXCEL_FILTER,
                DefaultExt = Properties.Resources.EXCEL_DEFAULT_EXT,
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (saveFileDialog.ShowDialog() ?? false) {
                if (CurrentProductListId != null) {
                    Task.Factory.StartNew(
                        () => {
                            new ImgFileExporter(
                                i => {
                                    Application.Current.Dispatcher.Invoke(
                                        () => {
                                            Title = $"{Application.ResourceAssembly.GetName().Name}: " +
                                                    string.Format(Properties.Resources.EXPORTING, i);
                                        });
                                }).Export(
                                    saveFileDialog.FileName, CurrentProductListId.Value);
                        })
                        .ContinueWith(task => {
                            Task.Delay(TimeSpan.FromSeconds(5));

                            Application.Current.Dispatcher.Invoke(
                                () => { Title = Application.ResourceAssembly.GetName().Name; });
                        });
                }
            }
        }

        private bool CollecScanning() {
            CurrentScanning = null;

            long sku;

            if (!long.TryParse(ScanTextBox.Text, out sku)) {
                Log.Warn(Properties.Resources.ENTERED_UPC_IS_NOT_A_NUMBER + ": " + ScanTextBox.Text);
                MessageBox.Show(Properties.Resources.ENTERED_UPC_IS_NOT_A_NUMBER);
                CleanupScanPanel();

                return false;
            }

            if (CurrentProductListId == null) {
                MessageBox.Show(Properties.Resources.PLEASE_SELECT_PRODUCT_LIST);
                CleanupScanPanel();

                return false;
            }

            var mainProduct = ProductDao.GetByProductListIdAndSku(CurrentProductListId.Value, sku);
            var product = ProductDao.GetByUpc(sku);

            if (product == null) {
                AdditionalInfoTextBox.Text = Properties.Resources.PRODUCT_IS_NOT_FOUND;

                CurrentScanning = new Scanning {
                    Product = new Product {
                        Id = -1,
                        Upc = sku,
                        Color = ColorDao.Find(""),
                        ColorText = "",
                        CreatedBy = -1,
                        CreatedTimestamp = Utils.GetCurrentTimestamp(),
                        Size = SizeDao.Find(""),
                        SizeText = "",
                        DepartmentName = "",
                        Description = "",
                        Division = "",
                        ImageUrl = "",
                        VendorName = "",
                        VendorOrStyleNumber = ""
                    },
                    Picture = "",
                    Duplicate = false,
                    AdditionalInfo = AdditionalInfoTextBox.Text
                };

                return false;
            }

            var sumOfDuplicates = _scannings.Sum(scanning => (scanning.Product.Upc == sku) ? 1 : 0);

            PictureTextBox.Text = (sumOfDuplicates == 0 && mainProduct != null) ? mainProduct.ImageUrl : "";

            if (PreviousSku != null && PreviousSku.Value == sku) {
                DuplicateTextBox.Text = Properties.Resources.DUPLICATE;
            }

            ColorTextBox.Text = product.ColorText;
            SizeTextBox.Text = product.SizeText;

            CurrentScanning = new Scanning {
                Product = product,
                Picture = PictureTextBox.Text,
                Duplicate = PreviousSku != null && PreviousSku.Value == sku
            };

            return true;
        }

        private void SaveRawProductInfo(Product product) {
            var saved = ProductDao.Save(product);
            var productList = ProductListDao.GetById(CurrentProductListId);

            if (productList == null) {
                Console.Error.WriteLine(Properties.Resources.PRODUCT_LIST_IS_NOT_FOUND);
                return;
            }

            ProductToProductListLinkDao.Save(new ProductToProductListLink {
                Id = -1,
                ProductList = productList,
                CreatedBy = -1,
                CreatedTimestamp = Utils.GetCurrentTimestamp(),
                ProductInfo = new ProductInfo {
                    Product = saved,
                    OriginalCost = 0m,
                    ClientCost = 0m,
                    Quantity = 1,
                    OriginalRetail = 0m
                }
            });

            ProductsRelationDao.Create(new List<ProductsRelation> {
                new ProductsRelation {
                    MainProduct = saved,
                    ProductList = productList,
                    ProductsRelationParts = new List<ProductsRelation.ProductsRelationPart> {
                        new ProductsRelation.ProductsRelationPart {
                            Id = -1,
                            RelatedProduct = saved,
                            CreatedBy = -1,
                            CreatedTimestamp = Utils.GetCurrentTimestamp()
                        }
                    }
                }
            });
        }

        private void SaveAndCleanCurrentScanning() {
            _scannings.Add(CurrentScanning);
            PreviousSku = CurrentScanning.Product.Upc;
            CurrentScanning = null;
        }

        private void ScanTextBox_KeyUp(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                Console.Out.WriteLine("Enter");
                if (CollecScanning()) {
                    if (CurrentScanning != null) {
                        SaveAndCleanCurrentScanning();
                    }

                    CleanupScanPanel();
                } else {
                    if (AdditionalInfoTextBox.Text.Equals(Properties.Resources.PRODUCT_IS_NOT_FOUND)) {
                        CurrentScanning.AdditionalInfo = Properties.Resources.PRODUCT_IS_NOT_FOUND_NEW_SAVED;
                        SaveRawProductInfo(CurrentScanning.Product);
                        SaveAndCleanCurrentScanning();
                    } else {
                        CurrentScanning = null;
                    }

                    CleanupScanPanel();
                }
            } else if (e.Key == Key.Escape) {
                Console.Out.WriteLine("ESC");
                CleanupScanPanel();
                CurrentScanning = null;
            } else if (ScanTextBox.Text.Length > 3) {
            }
        }

        private void AdditionalInfoTextBox_KeyUp(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                Console.Out.WriteLine("Enter");
                CollecScanning();
            } else if (e.Key == Key.Escape) {
                Console.Out.WriteLine("ESC");
                CleanupScanPanel();
                CurrentScanning = null;
            }
        }

        private void CleanupScanPanel() {
            ScanTextBox.Text = "";
            PictureTextBox.Text = "";
            DuplicateTextBox.Text = "";
            ColorTextBox.Text = "";
            SizeTextBox.Text = "";
            AdditionalInfoTextBox.Text = "";
        }

        private enum ApplicationState {
            ProductLists,
            Scanning,
            Started
        }
    }
}