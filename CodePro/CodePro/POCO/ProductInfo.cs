﻿namespace CodePro.POCO {
    public class ProductInfo {
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal OriginalCost { get; set; }
        public decimal OriginalRetail { get; set; }
        public decimal ClientCost { get; set; }
    }
}