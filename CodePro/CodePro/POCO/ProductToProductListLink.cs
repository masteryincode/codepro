﻿namespace CodePro.POCO {
    public class ProductToProductListLink {
        public int Id { get; set; }
        public ProductInfo ProductInfo { get; set; }
        public ProductList ProductList { get; set; }
        public int CreatedBy { get; set; }
        public int CreatedTimestamp { get; set; }
        public int? DeletedBy { get; set; }
        public int? DeletedTimestamp { get; set; }
        public bool IsDeleted => DeletedTimestamp == null;
    }
}