﻿using System.Collections.Generic;

namespace CodePro.POCO {
    public class ProductsRelation {
        public ProductList ProductList { get; set; }
        public Product MainProduct { get; set; }
        public List<ProductsRelationPart> ProductsRelationParts { get; set; }

        public class ProductsRelationPart {
            public int Id { get; set; }
            public Product RelatedProduct { get; set; }
            public int CreatedBy { get; set; }
            public int CreatedTimestamp { get; set; }
            public int? DeletedBy { get; set; }
            public int? DeletedTimestamp { get; set; }
        }
    }
}