﻿namespace CodePro.POCO {
    public class Color {
        public Color(string name) {
            Id = -1;
            Name = name;
            Fullname = name;
            Synonym1 = "";
            Synonym2 = "";
            Synonym3 = "";
            CreatedBy = -1;
            CreatedTimestamp = Utils.GetCurrentTimestamp();
        }

        public Color(int id, string name, string fullname, string synonym1, string synonym2, string synonym3,
            int createdBy, int createdTimestamp, int? deletedBy, int? deletedTimestamp) {
            Id = id;
            Name = name;
            Fullname = fullname;
            Synonym1 = synonym1;
            Synonym2 = synonym2;
            Synonym3 = synonym3;
            CreatedBy = createdBy;
            CreatedTimestamp = createdTimestamp;
            DeletedBy = deletedBy;
            DeletedTimestamp = deletedTimestamp;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Fullname { get; set; }
        public string Synonym1 { get; set; }
        public string Synonym2 { get; set; }
        public string Synonym3 { get; set; }
        public int CreatedBy { get; set; }
        public int CreatedTimestamp { get; set; }
        public int? DeletedBy { get; set; }
        public int? DeletedTimestamp { get; set; }
        public bool IsDeleted => DeletedTimestamp == null;
    }
}