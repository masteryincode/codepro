﻿namespace CodePro.POCO {
    public class Product {
        public int Id { get; set; }
        public long Upc { get; set; }
        public string Description { get; set; }
        public string VendorOrStyleNumber { get; set; }
        public string SizeText { get; set; }
        public Size Size { get; set; }
        public string ColorText { get; set; }
        public Color Color { get; set; }
        public string Division { get; set; }
        public string DepartmentName { get; set; }
        public string VendorName { get; set; }
        public string ImageUrl { get; set; }
        public int CreatedBy { get; set; }
        public int CreatedTimestamp { get; set; }
        public int? DeletedBy { get; set; }
        public int? DeletedTimestamp { get; set; }
        public bool IsDeleted => DeletedTimestamp == null;
    }
}