﻿namespace CodePro.POCO {
    public class Scanning {
        public Product Product { get; set; }
        public string Picture { get; set; }
        public bool Duplicate { get; set; }
        public string AdditionalInfo { get; set; }
    }
}