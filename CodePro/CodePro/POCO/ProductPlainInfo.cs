﻿namespace CodePro.POCO {
    public class ProductPlainInfo {
        public long Upc { get; set; }
        public string ItemDescription { get; set; }
        public int OriginalQuantity { get; set; }
        public decimal OriginalCost { get; set; }
        public decimal OriginalRetail { get; set; }
        public string VendorOrStyleNumber { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public decimal ClientCost { get; set; }
        public string Division { get; set; }
        public string DepartmentName { get; set; }
        public string VendorName { get; set; }
        public string Image { get; set; }
    }
}