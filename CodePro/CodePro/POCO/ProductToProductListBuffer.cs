﻿namespace CodePro.POCO {
    public class ProductToProductListBuffer {
        public int Id { get; set; }
        public string ProductListName { get; set; }
        public long Upc { get; set; }
        public string Description { get; set; }
        public int OriginalQuantity { get; set; }
        public decimal OriginalCost { get; set; }
        public decimal OriginalRetail { get; set; }
        public string VendorOrStyleNumber { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public decimal ClientCost { get; set; }
        public string Division { get; set; }
        public string DepartmentName { get; set; }
        public string VendorName { get; set; }
        public string ImageUrl { get; set; }
        public string Status { get; set; }
        public string ProcessingResult { get; set; }
        public int CreatedBy { get; set; }
        public int CreatedTimestamp { get; set; }
        public int? DeletedBy { get; set; }
        public int? DeletedTimestamp { get; set; }
        public bool IsDeleted => DeletedTimestamp == null;
    }
}