﻿using System;
using System.Globalization;
using System.Linq;

namespace CodePro.POCO {
    public class ProductList {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CreatedBy { get; set; }
        public int CreatedTimestamp { get; set; }
        public int? DeletedBy { get; set; }
        public int? DeletedTimestamp { get; set; }
        public bool IsDeleted => DeletedTimestamp == null;

        public string UserFriendlyName {
            get {
                var splitted = Name.Split();
                var dateTime =
                    DateTimeOffset.FromUnixTimeSeconds(long.Parse(splitted.Last()))
                        .LocalDateTime.ToString(CultureInfo.CurrentCulture);

                return string.Join(" ", splitted.Reverse().Skip(1).Reverse()) + $" [{dateTime}]";
            }
        }
    }
}