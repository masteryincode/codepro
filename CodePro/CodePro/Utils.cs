﻿using System;
using NLog;

namespace CodePro {
    public static class Utils {
        private static readonly Logger Log = LogManager.GetLogger("Utils.ParseCurrency");

        public static int GetCurrentTimestamp() {
            return (int) new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds();
        }

        public static decimal ParseCurrency(int row, int column, string text) {
            try {
                return decimal.Parse(text);
            } catch (ArgumentNullException argumentNullException) {
                Log.Debug("Argument '{0}' is null. Row: {1}, column: {2}. Exception: {3}", text, row, column,
                    argumentNullException);
                return 0.0m;
            } catch (FormatException formatException) {
                Log.Debug("Wrong format - '{0}'. Row: {1}, column: {2}. Exception: {3}'", text, row, column,
                    formatException);
                return 0.0m;
            } catch (OverflowException overflowException) {
                Log.Debug("Decimal overflow - '{0}'. Row: {1}, column: {2}. Exception: {3}'", text, row, column,
                    overflowException);
                return 0.0m;
            }
        }
    }
}