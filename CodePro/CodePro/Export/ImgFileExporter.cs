﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CodePro.DAO;
using NLog;
using OfficeOpenXml;

namespace CodePro.Export {
    public class ImgFileExporter {
        private static readonly Logger Log = LogManager.GetLogger("ImgFileExporter");
        private readonly Action<int> _onNext;

        public ImgFileExporter(Action<int> onNext) {
            _onNext = onNext;
        }

        public void Export(string fileName, int productListId) {
            using (var excelPackage = new ExcelPackage(new FileInfo(fileName))) {
                var sw = new Stopwatch();
                sw.Start();
                Log.Info(@"The Img file '{0}' exporting is started", fileName);

                if (excelPackage.Workbook.Worksheets["img"] != null) {
                    excelPackage.Workbook.Worksheets.Delete("img");
                }

                var worksheet = excelPackage.Workbook.Worksheets.Add("img");

                _onNext(0);
                var productsRelations = ProductsRelationDao.GetByProductListId(productListId);
                _onNext(10);

                if (productsRelations.Count == 0) {
                    excelPackage.Save();
                    _onNext(100);

                    sw.Stop();
                    Log.Info(@"The Img file exporting is stopped. Elapsed={0} Exported main products={1}", sw.Elapsed, 0);
                    Log.Info(@"Total exported={0}", 0);
                    return;
                }

                worksheet.InsertRow(1, productsRelations.Count);

                var maxColumns = productsRelations.Max(relation => relation.ProductsRelationParts.Count);

                worksheet.InsertColumn(1, maxColumns);
                var progress = 10;
                var totalExported = 0L;
                for (var i = 0; i < productsRelations.Count; ++i) {
                    var list = new List<string> {productsRelations[i].MainProduct.Upc.ToString()};

                    list.AddRange(productsRelations[i].ProductsRelationParts
                        .Where(part => part.RelatedProduct.Id != productsRelations[i].MainProduct.Id)
                        .Select(part => part.RelatedProduct.Upc.ToString()));

                    totalExported += list.Count;

                    for (var j = 0; j < list.Count; ++j) {
                        worksheet.Cells[i + 1, j + 1].Value = list[j];
                    }

                    worksheet.Cells[i + 1, maxColumns + 1].Value = productsRelations[i].MainProduct.ImageUrl;

                    var currentProgress = 10 + (int) ((i * 80.0) / productsRelations.Count);

                    if (currentProgress > progress) {
                        _onNext(currentProgress);

                        progress = currentProgress;
                    }
                }

                if (progress < 90) {
                    _onNext(90);
                }

                for (var i = 1; i <= maxColumns; ++i) {
                    worksheet.Column(i).AutoFit();
                }

                excelPackage.Save();
                sw.Stop();
                Log.Info(@"The Img file exporting is stopped. Elapsed={0} Exported main products={1}", sw.Elapsed,
                    productsRelations.Count);
                Log.Info(@"Total exported={0}", totalExported);
                _onNext(100);
            }
        }
    }
}