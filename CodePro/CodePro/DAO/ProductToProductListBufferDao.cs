﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class ProductToProductListBufferDao {
        private const string CreateQuery =
            "INSERT INTO product_to_product_list_buffer (product_list_name, upc, description, original_quantity, original_cost, " +
            "original_retail, vendor_or_style_number, color, size, client_cost, division, department_name, vendor_name, " +
            "image_url, status, processing_result, created_timestamp, created_by) VALUES (@product_list_name, @upc, @description, " +
            "@original_quantity, @original_cost, @original_retail, @vendor_or_style_number, @color, @size, " +
            "@client_cost, @division, @department_name, @vendor_name, @image_url, @status, @processing_result, @created_timestamp, -1)";

        private const string SaveQuery =
            "UPDATE product_to_product_list_buffer SET product_list_name = @product_list_name, upc = @upc, description = @description, " +
            "original_quantity = @original_quantity, original_cost = @original_cost, original_retail = @original_retail, " +
            "vendor_or_style_number = @vendor_or_style_number, color = @color, size = @size, client_cost = @client_cost, " +
            "division = @division, department_name = @department_name, vendor_name = @vendor_name, image_url = @image_url, " +
            "status = @status, processing_result = @processing_result WHERE id = @id AND deleted_timestamp IS NULL";

        private static readonly Logger Log = LogManager.GetLogger("ProductToProductListBufferDao");

        public static ProductToProductListBuffer Save(ProductToProductListBuffer buffer) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (buffer.Id == -1) {
                        command.CommandText = CreateQuery;
                        command.Parameters.AddWithValue("@created_timestamp", buffer.CreatedTimestamp);
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", buffer.Id);
                    }

                    command.Parameters.AddWithValue("@product_list_name", buffer.ProductListName);
                    command.Parameters.AddWithValue("@upc", buffer.Upc);
                    command.Parameters.AddWithValue("@description", buffer.Description);
                    command.Parameters.AddWithValue("@original_quantity", buffer.OriginalQuantity);
                    command.Parameters.AddWithValue("@original_cost", buffer.OriginalCost);
                    command.Parameters.AddWithValue("@original_retail", buffer.OriginalRetail);
                    command.Parameters.AddWithValue("@vendor_or_style_number", buffer.VendorOrStyleNumber);
                    command.Parameters.AddWithValue("@color", buffer.Color);
                    command.Parameters.AddWithValue("@size", buffer.Size);
                    command.Parameters.AddWithValue("@client_cost", buffer.ClientCost);
                    command.Parameters.AddWithValue("@division", buffer.Division);
                    command.Parameters.AddWithValue("@department_name", buffer.DepartmentName);
                    command.Parameters.AddWithValue("@vendor_name", buffer.VendorName);
                    command.Parameters.AddWithValue("@image_url", buffer.ImageUrl);
                    command.Parameters.AddWithValue("@status", buffer.Status);
                    command.Parameters.AddWithValue("@processing_result", buffer.ProcessingResult);
                    command.ExecuteNonQuery();

                    tx.Commit();

                    if (buffer.Id != -1) {
                        return buffer;
                    }

                    buffer.Id = (int) command.LastInsertedId;
                    buffer.CreatedBy = -1;

                    return buffer;
                } catch (MySqlException e) {
                    if (command.CommandText.Equals(CreateQuery)) {
                        buffer.Id = -1;
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return null;
                }
            }
        }

        public static bool SaveAll(List<ProductToProductListBuffer> buffers) {
            if (buffers.Count == 0) {
                return true;
            }

            using (var conn = DB.Utils.GetConnection()) {
                var createMode = buffers.First().Id == -1;
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (createMode) {
                        command.CommandText = CreateQuery;
                        command.Parameters.AddWithValue("@created_timestamp", 0);
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", 0);
                    }

                    command.Parameters.AddWithValue("@product_list_name", "");
                    command.Parameters.AddWithValue("@upc", "");
                    command.Parameters.AddWithValue("@description", "");
                    command.Parameters.AddWithValue("@original_quantity", 0);
                    command.Parameters.AddWithValue("@original_cost", 0m);
                    command.Parameters.AddWithValue("@original_retail", 0m);
                    command.Parameters.AddWithValue("@vendor_or_style_number", "");
                    command.Parameters.AddWithValue("@color", "");
                    command.Parameters.AddWithValue("@size", "");
                    command.Parameters.AddWithValue("@client_cost", 0m);
                    command.Parameters.AddWithValue("@division", "");
                    command.Parameters.AddWithValue("@department_name", "");
                    command.Parameters.AddWithValue("@vendor_name", "");
                    command.Parameters.AddWithValue("@image_url", "");
                    command.Parameters.AddWithValue("@status", "NOT PROCESSED");
                    command.Parameters.AddWithValue("@processing_result", "");

                    command.Prepare();

                    var newIds = new List<int>();

                    foreach (var buffer in buffers) {
                        if (createMode) {
                            command.Parameters["@created_timestamp"].Value = buffer.CreatedTimestamp;
                        } else {
                            command.Parameters["@id"].Value = buffer.Id;
                        }

                        command.Parameters["@product_list_name"].Value = buffer.ProductListName;
                        command.Parameters["@upc"].Value = buffer.Upc;
                        command.Parameters["@description"].Value = buffer.Description;
                        command.Parameters["@original_quantity"].Value = buffer.OriginalQuantity;
                        command.Parameters["@original_cost"].Value = buffer.OriginalCost;
                        command.Parameters["@original_retail"].Value = buffer.OriginalRetail;
                        command.Parameters["@vendor_or_style_number"].Value = buffer.VendorOrStyleNumber;
                        command.Parameters["@color"].Value = buffer.Color;
                        command.Parameters["@size"].Value = buffer.Size;
                        command.Parameters["@client_cost"].Value = buffer.ClientCost;
                        command.Parameters["@division"].Value = buffer.Division;
                        command.Parameters["@department_name"].Value = buffer.DepartmentName;
                        command.Parameters["@vendor_name"].Value = buffer.VendorName;
                        command.Parameters["@image_url"].Value = buffer.ImageUrl;
                        command.Parameters["@status"].Value = buffer.Status;
                        command.Parameters["@processing_result"].Value = buffer.ProcessingResult;

                        command.ExecuteNonQuery();

                        if (createMode) {
                            newIds.Add((int) command.LastInsertedId);
                        }
                    }

                    tx.Commit();

                    if (!createMode) {
                        return true;
                    }

                    for (var i = 0; i < newIds.Count; ++i) {
                        buffers[i].Id = newIds[i];
                    }

                    return true;
                } catch (Exception e) {
                    if (createMode) {
                        foreach (var buffer in buffers) {
                            buffer.Id = -1;
                        }
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return false;
                }
            }
        }
    }
}