﻿using System;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class LoginDao {
        private const string GetByIdQuery = "SELECT * FROM login WHERE id = @id";
        private const string GetByNameQuery = "SELECT * FROM login WHERE name = @name";
        private const string CreateQuery = "INSERT INTO login (name) VALUES (@name)";
        private const string SaveQuery = "UPDATE login SET name = @name WHERE id = @id";
        private static readonly Logger Log = LogManager.GetLogger("LoginDao");

        private static Login CreateFromReader(MySqlDataReader reader) {
            return new Login {
                Id = reader.GetInt32("id"),
                Name = reader.GetString("name")
            };
        }

        public static Login GetByName(string name) {
            Login result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByNameQuery;
                    command.Parameters.AddWithValue("@name", name);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }

        public static Login Find(string name) {
            return GetByName(name);
        }

        public static Login GetById(int id) {
            Login result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByIdQuery;
                    command.Parameters.AddWithValue("@id", id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }

        public static Login Save(Login login) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (login.Id == -1) {
                        command.CommandText = CreateQuery;
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", login.Id);
                    }

                    command.Parameters.AddWithValue("@name", login.Name);
                    command.ExecuteNonQuery();
                    tx.Commit();

                    if (login.Id == -1) {
                        login.Id = (int) command.LastInsertedId;
                    }

                    return login;
                } catch (Exception e) {
                    if (command.CommandText.Equals(CreateQuery)) {
                        login.Id = -1;
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return login;
                }
            }
        }
    }
}