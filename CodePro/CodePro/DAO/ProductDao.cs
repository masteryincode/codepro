﻿using System;
using System.Collections.Generic;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class ProductDao {
        private const string GetByIdQuery = "SELECT * FROM product WHERE id = @id AND deleted_timestamp IS NULL";
        private const string GetByUpcQuery = "SELECT * FROM product WHERE upc = @upc AND deleted_timestamp IS NULL";

        private const string CreateQuery =
            "INSERT INTO product (upc, description, vendor_or_style_number, size_text, size_id, color_text, color_id, " +
            "division, department_name, vendor_name, image_url, created_timestamp, created_by) " +
            "VALUES (@upc, @description, @vendor_or_style_number, @size_text, @size_id, @color_text, @color_id, " +
            "@division, @department_name, @vendor_name, @image_url, @created_timestamp, -1)";

        private const string SaveQuery =
            "UPDATE product SET upc = @upc, description = @description, " +
            "vendor_or_style_number = @vendor_or_style_number, size_text = @size_text, size_id = @size_id, " +
            "color_text = @color_text, color_id = @color_id, division = @division, department_name = @department_name, " +
            "vendor_name = @vendor_name, image_url = @image_url WHERE id = @id AND deleted_timestamp IS NULL";

        private const string GetProductListsQuery =
            "SELECT pl.* FROM product_to_product_list AS p2pl " +
            "JOIN product_list AS pl ON pl.id = p2pl.product_list_id AND pl.deleted_timestamp IS NULL " +
            "JOIN product AS p ON p.id = p2pl.product_id AND p.deleted_timestamp IS NULL AND p.id = @product_id";

        private const string GetProductsPlainInfoByProductListQuery =
            "SELECT p.upc, p.description, p2pl.quantity, p2pl.original_cost, p2pl.original_retail, " +
            "p.vendor_or_style_number, p.color_text, p.size_text, p2pl.client_cost, p.division, " +
            "p.department_name, p.vendor_name, p.image_url FROM product AS p " +
            "JOIN product_to_product_list AS p2pl ON p.id = p2pl.product_id AND p2pl.product_list_id = @product_list_id " +
            "WHERE p.deleted_timestamp IS NULL";

        /*
        private const string GetByProductListIdAndSkuQuery =
            "SELECT p.* FROM product AS p JOIN products_relation AS pr " +
            "ON p.id = pr.main_product_id AND pr.product_list_id = @product_list_id " +
            "AND pr.deleted_timestamp IS NULL JOIN product AS p2 " +
            "ON p2.id = pr.related_product_id AND p2.upc = @sku AND p2.deleted_timestamp IS NULL " +
            "WHERE p.deleted_timestamp IS NULL LIMIT 1";*/

        private const string GetByProductListIdAndSkuQuery =
            "SELECT p.* FROM product AS p JOIN products_relation AS pr " +
            "ON p.id = pr.main_product_id AND pr.product_list_id = @product_list_id " +
            "AND pr.deleted_timestamp IS NULL WHERE p.upc = @sku AND p.deleted_timestamp IS NULL LIMIT 1";

        private static readonly Logger Log = LogManager.GetLogger("ProductDao");

        public static Product CreateFromReader(MySqlDataReader reader) {
            return new Product {
                Id = reader.GetInt32("id"),
                Upc = reader.GetInt64("upc"),
                Description = reader.GetString("description"),
                VendorOrStyleNumber = reader.GetString("vendor_or_style_number"),
                SizeText = reader.GetString("size_text"),
                Size = SizeDao.GetById(reader.GetInt32("size_id")),
                ColorText = reader.GetString("color_text"),
                Color = ColorDao.GetById(reader.GetInt32("color_id")),
                Division = reader.GetString("division"),
                DepartmentName = reader.GetString("department_name"),
                VendorName = reader.GetString("vendor_name"),
                ImageUrl = reader.GetString("image_url"),
                CreatedBy = reader.GetInt32("created_by"),
                CreatedTimestamp = reader.GetInt32("created_timestamp"),
                DeletedBy =
                    reader.IsDBNull(reader.GetOrdinal("deleted_by")) ? (int?) null : reader.GetInt32("deleted_by"),
                DeletedTimestamp =
                    reader.IsDBNull(reader.GetOrdinal("deleted_timestamp"))
                        ? (int?) null
                        : reader.GetInt32("deleted_timestamp")
            };
        }

        public static ProductInfo CreateInfoFromReader(MySqlDataReader reader) {
            return new ProductInfo {
                Product = CreateFromReader(reader),
                Quantity = reader.GetInt32("quantity"),
                OriginalCost = reader.GetDecimal("original_cost"),
                OriginalRetail = reader.GetDecimal("original_retail"),
                ClientCost = reader.GetDecimal("client_cost")
            };
        }

        public static ProductPlainInfo CreatePlainInfoFromReader(MySqlDataReader reader) {
            return new ProductPlainInfo {
                Upc = reader.GetInt64("upc"),
                ItemDescription = reader.GetString("description"),
                OriginalQuantity = reader.GetInt32("quantity"),
                OriginalCost = reader.GetDecimal("original_cost"),
                OriginalRetail = reader.GetDecimal("original_retail"),
                VendorOrStyleNumber = reader.GetString("vendor_or_style_number"),
                Color = reader.GetString("color_text"),
                Size = reader.GetString("size_text"),
                ClientCost = reader.GetDecimal("client_cost"),
                Division = reader.GetString("division"),
                DepartmentName = reader.GetString("department_name"),
                VendorName = reader.GetString("vendor_name"),
                Image = reader.GetString("image_url")
            };
        }

        public static Product GetById(int id) {
            Product result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByIdQuery;
                    command.Parameters.AddWithValue("@id", id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);

                            if (result.Color == null || result.Size == null) {
                                tx.Rollback();

                                return null;
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }

        public static Product GetByUpc(long upc) {
            Product result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByUpcQuery;
                    command.Parameters.AddWithValue("@upc", upc);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);

                            if (result.Color == null || result.Size == null) {
                                tx.Rollback();

                                return null;
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }

        public static Product Save(Product product) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (product.Id == -1) {
                        command.CommandText = CreateQuery;
                        command.Parameters.AddWithValue("@created_timestamp", product.CreatedTimestamp);
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", product.Id);
                    }

                    command.Parameters.AddWithValue("@upc", product.Upc);
                    command.Parameters.AddWithValue("@description", product.Description);
                    command.Parameters.AddWithValue("@vendor_or_style_number", product.VendorOrStyleNumber);
                    command.Parameters.AddWithValue("@size_text", product.SizeText);
                    command.Parameters.AddWithValue("@size_id", product.Size.Id);
                    command.Parameters.AddWithValue("@color_text", product.ColorText);
                    command.Parameters.AddWithValue("@color_id", product.Color.Id);
                    command.Parameters.AddWithValue("@division", product.Division);
                    command.Parameters.AddWithValue("@department_name", product.DepartmentName);
                    command.Parameters.AddWithValue("@vendor_name", product.VendorName);
                    command.Parameters.AddWithValue("@image_url", product.ImageUrl);
                    command.ExecuteNonQuery();
                    tx.Commit();

                    if (product.Id == -1) {
                        product.Id = (int) command.LastInsertedId;
                        product.CreatedBy = -1;
                    }

                    return product;
                } catch (Exception e) {
                    if (command.CommandText.Equals(CreateQuery)) {
                        product.Id = -1;
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return product;
                }
            }
        }

        public static bool Create(List<Product> products) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = CreateQuery;
                    command.Parameters.AddWithValue("@created_timestamp", 0);
                    command.Parameters.AddWithValue("@upc", "");
                    command.Parameters.AddWithValue("@description", "");
                    command.Parameters.AddWithValue("@vendor_or_style_number", "");
                    command.Parameters.AddWithValue("@size_text", "");
                    command.Parameters.AddWithValue("@size_id", 0);
                    command.Parameters.AddWithValue("@color_text", "");
                    command.Parameters.AddWithValue("@color_id", 0);
                    command.Parameters.AddWithValue("@division", "");
                    command.Parameters.AddWithValue("@department_name", "");
                    command.Parameters.AddWithValue("@vendor_name", "");
                    command.Parameters.AddWithValue("@image_url", "");

                    command.Prepare();

                    foreach (var p in products) {
                        command.Parameters["@created_timestamp"].Value = p.CreatedTimestamp;
                        command.Parameters["@upc"].Value = p.Upc;
                        command.Parameters["@description"].Value = p.Description;
                        command.Parameters["@vendor_or_style_number"].Value = p.VendorOrStyleNumber;
                        command.Parameters["@size_text"].Value = p.SizeText;
                        command.Parameters["@size_id"].Value = p.Size.Id;
                        command.Parameters["@color_text"].Value = p.ColorText;
                        command.Parameters["@color_id"].Value = p.Color.Id;
                        command.Parameters["@division"].Value = p.Division;
                        command.Parameters["@department_name"].Value = p.DepartmentName;
                        command.Parameters["@vendor_name"].Value = p.VendorName;
                        command.Parameters["@image_url"].Value = p.ImageUrl;

                        command.ExecuteNonQuery();

                        p.Id = (int) command.LastInsertedId;
                    }

                    tx.Commit();

                    return true;
                } catch (MySqlException e) {
                    tx.Rollback();
                    Log.Error(e);

                    return false;
                }
            }
        }

        public static
            List<ProductList> GetProductLists(Product product) {
            var result = new List<ProductList>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetProductListsQuery;
                    command.Parameters.AddWithValue("@product_id", product.Id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(ProductListDao.CreateFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return new List<ProductList>();
                }
            }

            return result;
        }

        public static List<ProductPlainInfo> GetProductsPlainInfoByProductList(int productListId) {
            var result = new List<ProductPlainInfo>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetProductsPlainInfoByProductListQuery;
                    command.Parameters.AddWithValue("@product_list_id", productListId);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(CreatePlainInfoFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return new List<ProductPlainInfo>();
                }
            }

            return result;
        }

        public static Product GetByProductListIdAndSku(int productListId, long sku) {
            Product result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByProductListIdAndSkuQuery;
                    command.Parameters.AddWithValue("@product_list_id", productListId);
                    command.Parameters.AddWithValue("@sku", sku);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);

                            if (result.Color == null || result.Size == null) {
                                tx.Rollback();

                                return null;
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }
    }
}