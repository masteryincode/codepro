﻿using System;
using System.Collections.Generic;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class ProductToProductListLinkDao {
        private const string CreateQuery =
            "INSERT INTO product_to_product_list (product_id, product_list_id, quantity, original_cost, " +
            "original_retail, client_cost, created_timestamp, created_by) VALUES (@product_id, @product_list_id, @quantity, @original_cost, " +
            "@original_retail, @client_cost, @created_timestamp, -1)";

        private const string SaveQuery =
            "UPDATE product_to_product_list SET product_id = @product_id, product_list_id = @product_list_id, quantity = @quantity, " +
            "original_cost = @original_cost, original_retail = @original_retail, client_cost = @client_cost WHERE id = @id AND deleted_timestamp IS NULL";

        private const string GetByProductAndProductListQuery =
            "SELECT * FROM product_to_product_list WHERE product_id = @product_id AND product_list_id = @product_list_id AND deleted_timestamp IS NULL";

        private static readonly Logger Log = LogManager.GetLogger("ProductToProductListLinkDao");

        public static ProductToProductListLink CreateFromReader(MySqlDataReader reader) {
            return new ProductToProductListLink {
                Id = reader.GetInt32("id"),
                ProductInfo = new ProductInfo {
                    Product = ProductDao.GetById(reader.GetInt32("product_id")),
                    Quantity = reader.GetInt32("quantity"),
                    OriginalCost = reader.GetDecimal("original_cost"),
                    OriginalRetail = reader.GetDecimal("original_retail"),
                    ClientCost = reader.GetDecimal("client_cost")
                },
                ProductList = ProductListDao.GetById(reader.GetInt32("product_list_id")),
                CreatedBy = reader.GetInt32("created_by"),
                CreatedTimestamp = reader.GetInt32("created_timestamp"),
                DeletedBy =
                    reader.IsDBNull(reader.GetOrdinal("deleted_by")) ? (int?) null : reader.GetInt32("deleted_by"),
                DeletedTimestamp =
                    reader.IsDBNull(reader.GetOrdinal("deleted_timestamp"))
                        ? (int?) null
                        : reader.GetInt32("deleted_timestamp")
            };
        }

        public static ProductToProductListLink Save(ProductToProductListLink link) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (link.Id == -1) {
                        command.CommandText = CreateQuery;
                        command.Parameters.AddWithValue("@created_timestamp", link.CreatedTimestamp);
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", link.Id);
                    }

                    command.Parameters.AddWithValue("@product_id", link.ProductInfo.Product.Id);
                    command.Parameters.AddWithValue("@product_list_id", link.ProductList.Id);
                    command.Parameters.AddWithValue("@quantity", link.ProductInfo.Quantity);
                    command.Parameters.AddWithValue("@original_cost", link.ProductInfo.OriginalCost);
                    command.Parameters.AddWithValue("@original_retail", link.ProductInfo.OriginalRetail);
                    command.Parameters.AddWithValue("@client_cost", link.ProductInfo.ClientCost);
                    command.ExecuteNonQuery();
                    tx.Commit();

                    if (link.Id != -1) {
                        return link;
                    }

                    link.Id = (int) command.LastInsertedId;
                    link.CreatedBy = -1;

                    return link;
                } catch (Exception e) {
                    if (command.CommandText.Equals(CreateQuery)) {
                        link.Id = -1;
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return link;
                }
            }
        }

        public static bool Create(IEnumerable<ProductToProductListLink> links) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = CreateQuery;
                    command.Parameters.AddWithValue("@created_timestamp", 0);
                    command.Parameters.AddWithValue("@product_id", 0);
                    command.Parameters.AddWithValue("@product_list_id", 0);
                    command.Parameters.AddWithValue("@quantity", 0);
                    command.Parameters.AddWithValue("@original_cost", 0m);
                    command.Parameters.AddWithValue("@original_retail", 0m);
                    command.Parameters.AddWithValue("@client_cost", 0m);

                    command.Prepare();

                    foreach (var l in links) {
                        command.Parameters["@created_timestamp"].Value = l.CreatedTimestamp;
                        command.Parameters["@product_id"].Value = l.ProductInfo.Product.Id;
                        command.Parameters["@product_list_id"].Value = l.ProductList.Id;
                        command.Parameters["@quantity"].Value = l.ProductInfo.Quantity;
                        command.Parameters["@original_cost"].Value = l.ProductInfo.OriginalCost;
                        command.Parameters["@original_retail"].Value = l.ProductInfo.OriginalRetail;
                        command.Parameters["@client_cost"].Value = l.ProductInfo.ClientCost;

                        command.ExecuteNonQuery();

                        l.Id = (int) command.LastInsertedId;
                    }

                    tx.Commit();
                    return true;
                } catch (MySqlException e) {
                    tx.Rollback();
                    Log.Error(e);

                    return false;
                }
            }
        }

        public static ProductToProductListLink GetByProductAndProductList(Product product, ProductList list) {
            ProductToProductListLink result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByProductAndProductListQuery;
                    command.Parameters.AddWithValue("@product_id", product.Id);
                    command.Parameters.AddWithValue("@product_list_id", list.Id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);

                            if (result.ProductInfo == null || result.ProductList == null) {
                                tx.Rollback();

                                return null;
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return null;
                }
            }

            return result;
        }
    }
}