﻿using System;
using System.Collections.Generic;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class ColorDao {
        private const string GetByIdQuery = "SELECT * FROM color WHERE id = @id AND deleted_timestamp IS NULL";
        private const string GetByNameQuery = "SELECT * FROM color WHERE name = @name AND deleted_timestamp IS NULL";

        private const string GetByFullNameQuery =
            "SELECT * FROM color WHERE fullname = @fullname AND deleted_timestamp IS NULL";

        private const string GetBySynonymQuery =
            "SELECT * FROM color WHERE (synonym_1 = @synonym_1 OR synonym_2 = @synonym_2 OR synonym_3 = @synonym_3) " +
            "AND deleted_timestamp IS NULL";

        private const string CreateQuery =
            "INSERT INTO color (name, fullname, synonym_1, synonym_2, synonym_3, created_timestamp, created_by) " +
            "VALUES (@name, @fullname, @synonym_1, @synonym_2, @synonym_3, @created_timestamp, -1)";

        private const string SaveQuery =
            "UPDATE color SET name = @name, fullname = @fullname, synonym_1 = @synonym_1, synonym_2 = @synonym_2, " +
            "synonym_3 = @synonym_3 WHERE id = @id AND deleted_timestamp IS NULL";

        private static readonly Logger Log = LogManager.GetLogger("ColorDao");

        private static Color CreateFromReader(MySqlDataReader reader) {
            return new Color(reader.GetInt32("id"), reader.GetString("name"), reader.GetString("fullname"),
                reader.GetString("synonym_1"), reader.GetString("synonym_2"), reader.GetString("synonym_3"),
                reader.GetInt32("created_by"), reader.GetInt32("created_timestamp"),
                reader.IsDBNull(reader.GetOrdinal("deleted_by")) ? (int?) null : reader.GetInt32("deleted_by"),
                reader.IsDBNull(reader.GetOrdinal("deleted_timestamp"))
                    ? (int?) null
                    : reader.GetInt32("deleted_timestamp"));
        }

        public static Color GetByName(string name) {
            Color result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByNameQuery;
                    command.Parameters.AddWithValue("@name", name);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }

        public static Color GetByFullName(string fullName) {
            Color result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByFullNameQuery;
                    command.Parameters.AddWithValue("@fullname", fullName);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }

        public static List<Color> GetBySynonym(string synonym) {
            var result = new List<Color>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetBySynonymQuery;
                    command.Parameters.AddWithValue("@synonym_1", synonym);
                    command.Parameters.AddWithValue("@synonym_2", synonym);
                    command.Parameters.AddWithValue("@synonym_3", synonym);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(CreateFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return new List<Color>();
                }
            }

            return result;
        }

        public static Color Find(string name) {
            var color = GetByName(name);

            if (color != null) {
                return color;
            }

            color = GetByFullName(name);

            if (color != null) {
                return color;
            }

            var colors = GetBySynonym(name);

            return colors.Count > 0 ? colors[0] : null;
        }

        public static Color GetById(int id) {
            Color result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByIdQuery;
                    command.Parameters.AddWithValue("@id", id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);
                    return null;
                }
            }

            return result;
        }

        public static Color Save(Color color) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (color.Id == -1) {
                        command.CommandText = CreateQuery;
                        command.Parameters.AddWithValue("@created_timestamp", color.CreatedTimestamp);
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", color.Id);
                    }

                    command.Parameters.AddWithValue("@name", color.Name);
                    command.Parameters.AddWithValue("@fullname", color.Fullname);
                    command.Parameters.AddWithValue("@synonym_1", color.Synonym1);
                    command.Parameters.AddWithValue("@synonym_2", color.Synonym2);
                    command.Parameters.AddWithValue("@synonym_3", color.Synonym3);
                    command.ExecuteNonQuery();
                    tx.Commit();

                    if (color.Id == -1) {
                        color.Id = (int) command.LastInsertedId;
                        color.CreatedBy = -1;
                    }

                    return color;
                } catch (Exception e) {
                    if (command.CommandText.Equals(CreateQuery)) {
                        color.Id = -1;
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return color;
                }
            }
        }
    }
}