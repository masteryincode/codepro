﻿using System;
using System.Collections.Generic;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class SizeDao {
        private const string GetByIdQuery = "SELECT * FROM size WHERE id = @id AND deleted_timestamp IS NULL";
        private const string GetByNameQuery = "SELECT * FROM size WHERE name = @name AND deleted_timestamp IS NULL";

        private const string GetByFullNameQuery =
            "SELECT * FROM size WHERE fullname = @fullname AND deleted_timestamp IS NULL";

        private const string GetBySynonymQuery =
            "SELECT * FROM size WHERE (synonym_1 = @synonym_1 OR synonym_2 = @synonym_2 OR synonym_3 = @synonym_3) " +
            "AND deleted_timestamp IS NULL";

        private const string GetByNumberQuery =
            "SELECT * FROM size WHERE number = @number AND is_numeric = TRUE AND deleted_timestamp IS NULL";

        private const string CreateQuery =
            "INSERT INTO size (name, fullname, number, is_numeric, synonym_1, synonym_2, synonym_3, created_timestamp, created_by) " +
            "VALUES (@name, @fullname, @number, @is_numeric, @synonym_1, @synonym_2, @synonym_3, @created_timestamp, -1)";

        private const string SaveQuery =
            "UPDATE size SET name = @name, fullname = @fullname, number = @number, is_numeric = @is_numeric, " +
            "synonym_1 = @synonym_1, synonym_2 = @synonym_2, synonym_3 = @synonym_3 WHERE id = @id AND deleted_timestamp IS NULL";

        private static readonly Logger Log = LogManager.GetLogger("ProductToProductListLinkDao");

        private static Size CreateFromReader(MySqlDataReader reader) {
            return new Size(reader.GetInt32("id"), reader.GetString("name"), reader.GetString("fullname"),
                reader.GetInt32("number"),
                reader.GetBoolean("is_numeric"), reader.GetString("synonym_1"), reader.GetString("synonym_2"),
                reader.GetString("synonym_3"),
                reader.GetInt32("created_by"), reader.GetInt32("created_timestamp"),
                reader.IsDBNull(reader.GetOrdinal("deleted_by")) ? (int?) null : reader.GetInt32("deleted_by"),
                reader.IsDBNull(reader.GetOrdinal("deleted_timestamp"))
                    ? (int?) null
                    : reader.GetInt32("deleted_timestamp"));
        }

        public static Size GetByName(string name) {
            Size result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByNameQuery;
                    command.Parameters.AddWithValue("@name", name);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return null;
                }
            }

            return result;
        }

        public static Size GetByFullName(string fullname) {
            Size result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByFullNameQuery;
                    command.Parameters.AddWithValue("@fullname", fullname);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return null;
                }
            }

            return result;
        }

        public static List<Size> GetBySynonym(string synonym) {
            var result = new List<Size>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetBySynonymQuery;
                    command.Parameters.AddWithValue("@synonym_1", synonym);
                    command.Parameters.AddWithValue("@synonym_2", synonym);
                    command.Parameters.AddWithValue("@synonym_3", synonym);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(CreateFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return new List<Size>();
                }
            }

            return result;
        }

        public static Size GetByNumber(int number) {
            Size result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByNumberQuery;
                    command.Parameters.AddWithValue("@number", number);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return null;
                }
            }

            return result;
        }

        public static Size Find(string name) {
            var size = GetByName(name);

            if (size != null) {
                return size;
            }

            size = GetByFullName(name);

            if (size != null) {
                return size;
            }

            var sizes = GetBySynonym(name);

            return sizes.Count > 0 ? sizes[0] : null;
        }

        public static Size Find(int number) {
            return GetByNumber(number);
        }

        public static Size GetById(int id) {
            Size result = null;

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByIdQuery;
                    command.Parameters.AddWithValue("@id", id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return null;
                }
            }

            return result;
        }

        public static Size Save(Size size) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (size.Id == -1) {
                        command.CommandText = CreateQuery;
                        command.Parameters.AddWithValue("@created_timestamp", size.CreatedTimestamp);
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", size.Id);
                    }

                    command.Parameters.AddWithValue("@name", size.Name);
                    command.Parameters.AddWithValue("@fullname", size.Fullname);
                    command.Parameters.AddWithValue("@number", size.Number);
                    command.Parameters.AddWithValue("@is_numeric", size.IsNumeric);
                    command.Parameters.AddWithValue("@synonym_1", size.Synonym1);
                    command.Parameters.AddWithValue("@synonym_2", size.Synonym2);
                    command.Parameters.AddWithValue("@synonym_3", size.Synonym3);
                    command.ExecuteNonQuery();
                    tx.Commit();

                    if (size.Id != -1) {
                        return size;
                    }

                    size.Id = (int) command.LastInsertedId;
                    size.CreatedBy = -1;

                    return size;
                } catch (Exception e) {
                    if (command.CommandText.Equals(CreateQuery)) {
                        size.Id = -1;
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return size;
                }
            }
        }
    }
}