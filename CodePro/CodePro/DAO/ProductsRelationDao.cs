﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class ProductsRelationDao {
        private const string CreateQuery =
            "INSERT INTO products_relation (product_list_id, main_product_id, related_product_id, sequence_order, created_timestamp, " +
            "created_by) VALUES (@product_list_id, @main_product_id, @related_product_id, @sequence_order, @created_timestamp, -1)";

        private const string GetByProductListIdQuery =
            "SELECT * FROM products_relation WHERE product_list_id = @product_list_id AND deleted_timestamp IS NULL " +
            "ORDER BY main_product_id, sequence_order";

        private static readonly Logger Log = LogManager.GetLogger("ProductsRelationDao");

        private static List<ProductsRelation> CreateListFromReader(MySqlDataReader reader, int productListId) {
            var mainProductIdToProductsRelationPart = new Dictionary<int, List<ProductsRelation.ProductsRelationPart>>();
            var productList = ProductListDao.GetById(productListId);

            while (reader.Read()) {
                var id = reader.GetInt32("id");
                var mainId = reader.GetInt32("main_product_id");
                var relatedId = reader.GetInt32("related_product_id");
                var createdBy = reader.GetInt32("created_by");
                var createdTimestamp = reader.GetInt32("created_timestamp");
                var deletedBy =
                    reader.IsDBNull(reader.GetOrdinal("deleted_by")) ? (int?) null : reader.GetInt32("deleted_by");
                var deletedTimestamp =
                    reader.IsDBNull(reader.GetOrdinal("deleted_timestamp"))
                        ? (int?) null
                        : reader.GetInt32("deleted_timestamp");

                var productsRelationPart = new ProductsRelation.ProductsRelationPart {
                    Id = id,
                    RelatedProduct = ProductDao.GetById(relatedId),
                    CreatedBy = createdBy,
                    CreatedTimestamp = createdTimestamp,
                    DeletedBy = deletedBy,
                    DeletedTimestamp = deletedTimestamp
                };

                if (mainProductIdToProductsRelationPart.ContainsKey(mainId)) {
                    mainProductIdToProductsRelationPart[mainId].Add(productsRelationPart);
                } else {
                    mainProductIdToProductsRelationPart
                        .Add(mainId, new List<ProductsRelation.ProductsRelationPart> {productsRelationPart});
                }
            }

            return mainProductIdToProductsRelationPart.Keys.Select(mainProductId => new ProductsRelation {
                MainProduct = ProductDao.GetById(mainProductId),
                ProductList = productList,
                ProductsRelationParts = mainProductIdToProductsRelationPart[mainProductId]
            }).ToList();
        }

        public static bool Create(List<ProductsRelation> relations) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = CreateQuery;
                    command.Parameters.AddWithValue("@created_timestamp", 0);
                    command.Parameters.AddWithValue("@product_list_id", 0);
                    command.Parameters.AddWithValue("@main_product_id", 0);
                    command.Parameters.AddWithValue("@related_product_id", 0);
                    command.Parameters.AddWithValue("@sequence_order", 0);

                    command.Prepare();

                    foreach (var r in relations) {
                        for (var i = 0; i < r.ProductsRelationParts.Count; ++i) {
                            var part = r.ProductsRelationParts[i];

                            command.Parameters["@created_timestamp"].Value = part.CreatedTimestamp;
                            command.Parameters["@product_list_id"].Value = r.ProductList.Id;
                            command.Parameters["@main_product_id"].Value = r.MainProduct.Id;
                            command.Parameters["@related_product_id"].Value = part.RelatedProduct.Id;
                            command.Parameters["@sequence_order"].Value = i;

                            command.ExecuteNonQuery();

                            part.Id = (int) command.LastInsertedId;
                        }
                    }

                    tx.Commit();
                    return true;
                } catch (MySqlException e) {
                    tx.Rollback();
                    Log.Error(e);

                    return false;
                }
            }
        }

        public static List<ProductsRelation> GetByProductListId(int productListId) {
            var result = new List<ProductsRelation>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByProductListIdQuery;
                    command.Parameters.AddWithValue("@product_list_id", productListId);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            result.AddRange(CreateListFromReader(reader, productListId));
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return new List<ProductsRelation>();
                }
            }

            return result;
        }
    }
}