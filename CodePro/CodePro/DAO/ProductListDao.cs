﻿using System;
using System.Collections.Generic;
using CodePro.POCO;
using MySql.Data.MySqlClient;
using NLog;

namespace CodePro.DAO {
    public static class ProductListDao {
        private const string CreateQuery =
            "INSERT INTO product_list (name, created_timestamp, created_by) VALUES (@name, @created_timestamp, -1)";

        private const string SaveQuery =
            "UPDATE product_list SET name = @name WHERE id = @id AND deleted_timestamp IS NULL";

        private const string GetByIdQuery =
            "SELECT * FROM product_list WHERE id = @id AND deleted_timestamp IS NULL";

        private const string GetQuery =
            "SELECT * FROM product_list WHERE deleted_timestamp IS NULL ORDER BY created_timestamp";

        private const string GetByNameQuery =
            "SELECT * FROM product_list WHERE name = @name AND deleted_timestamp IS NULL";

        private const string GetProductInfoQuery =
            "SELECT p.*, p2pl.quantity, p2pl.original_cost, p2pl.original_retail, p2pl.client_cost FROM product_to_product_list AS p2pl " +
            "JOIN product_list AS pl ON pl.id = p2pl.product_list_id AND pl.deleted_timestamp IS NULL AND pl.id = @product_list_id " +
            "JOIN product AS p ON p.id = p2pl.product_id AND p.deleted_timestamp IS NULL";

        private const string GetProductsQuery =
            "SELECT p.* FROM product_to_product_list AS p2pl " +
            "JOIN product_list AS pl ON pl.id = p2pl.product_list_id AND pl.deleted_timestamp IS NULL AND pl.id = @product_list_id " +
            "JOIN product AS p ON p.id = p2pl.product_id AND p.deleted_timestamp IS NULL";

        private static readonly Logger Log = LogManager.GetLogger("ProductListDao");

        public static ProductList CreateFromReader(MySqlDataReader reader) {
            return new ProductList {
                Id = reader.GetInt32("id"),
                Name = reader.GetString("name"),
                CreatedBy = reader.GetInt32("created_by"),
                CreatedTimestamp = reader.GetInt32("created_timestamp"),
                DeletedBy =
                    reader.IsDBNull(reader.GetOrdinal("deleted_by")) ? (int?) null : reader.GetInt32("deleted_by"),
                DeletedTimestamp =
                    reader.IsDBNull(reader.GetOrdinal("deleted_timestamp"))
                        ? (int?) null
                        : reader.GetInt32("deleted_timestamp")
            };
        }

        public static ProductList GetById(int? id) {
            ProductList result = null;

            if (id == null) {
                return null;
            }

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByIdQuery;
                    command.Parameters.AddWithValue("@id", id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            reader.Read();
                            result = CreateFromReader(reader);
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return null;
                }
            }

            return result;
        }

        public static List<ProductList> GetByName(string name) {
            var result = new List<ProductList>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetByNameQuery;
                    command.Parameters.AddWithValue("@name", name);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(CreateFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return new List<ProductList>();
                }
            }

            return result;
        }

        public static ProductList Save(ProductList productList) {
            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    if (productList.Id == -1) {
                        command.CommandText = CreateQuery;
                        command.Parameters.AddWithValue("@created_timestamp", productList.CreatedTimestamp);
                    } else {
                        command.CommandText = SaveQuery;
                        command.Parameters.AddWithValue("@id", productList.Id);
                    }

                    command.Parameters.AddWithValue("@name", productList.Name);
                    command.ExecuteNonQuery();
                    tx.Commit();

                    if (productList.Id == -1) {
                        productList.Id = (int) command.LastInsertedId;
                        productList.CreatedBy = -1;
                    }

                    return productList;
                } catch (Exception e) {
                    if (command.CommandText.Equals(CreateQuery)) {
                        productList.Id = -1;
                    }

                    tx.Rollback();
                    Log.Error(e);

                    return productList;
                }
            }
        }

        public static List<Product> GetProducts(ProductList productList) {
            var result = new List<Product>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetProductsQuery;
                    command.Parameters.AddWithValue("@product_list_id", productList.Id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(ProductDao.CreateFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return new List<Product>();
                }
            }

            return result;
        }

        public static List<ProductInfo> GetProductInfo(ProductList productList) {
            var result = new List<ProductInfo>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetProductInfoQuery;
                    command.Parameters.AddWithValue("@product_list_id", productList.Id);

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(ProductDao.CreateInfoFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return new List<ProductInfo>();
                }
            }

            return result;
        }

        public static List<ProductList> Get() {
            var result = new List<ProductList>();

            using (var conn = DB.Utils.GetConnection()) {
                conn.Open();

                var tx = conn.BeginTransaction();
                var command = conn.CreateCommand();

                command.Transaction = tx;

                try {
                    command.CommandText = GetQuery;

                    using (var reader = command.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                result.Add(CreateFromReader(reader));
                            }
                        }
                    }

                    tx.Commit();
                } catch (Exception e) {
                    tx.Rollback();
                    Log.Error(e);

                    return new List<ProductList>();
                }
            }

            return result;
        }
    }
}