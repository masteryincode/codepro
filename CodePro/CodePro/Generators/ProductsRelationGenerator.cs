﻿using System.Collections.Generic;
using System.Linq;
using CodePro.POCO;

namespace CodePro.Generators {
    public static class ProductsRelationGenerator {
        private static readonly Dictionary<string, int> XsToXxxlSizesOrderingWeight = new Dictionary<string, int> {
            {"XS", 0},
            {"S", 1},
            {"M", 2},
            {"L", 3},
            {"XL", 4},
            {"XXL", 5},
            {"XXXL", 6}
        };

        public static ProductsRelation Generate(ProductList list, List<Product> products) {
            if (products.Count == 0) {
                return null;
            }

            var separated = SeparateBySizeCategories(products);
            Product main;
            var related = new List<Product>(separated[SizeCategory.XsToXxxl]);

            related.AddRange(separated[SizeCategory.Numeric]);
            related.AddRange(separated[SizeCategory.Other]);

            if (separated[SizeCategory.XsToXxxl].Count > 0) {
                var first = separated[SizeCategory.XsToXxxl][0];

                if (separated[SizeCategory.XsToXxxl].Count == 1 || !first.Size.Name.Equals("XS")) {
                    main = first;
                } else {
                    var second = separated[SizeCategory.XsToXxxl][1];

                    main = XsToXxxlSizesOrderingWeight[second.Size.Name] > 2 ? first : second;
                }
            } else if (separated[SizeCategory.Numeric].Count > 0) {
                main = separated[SizeCategory.Numeric][0];
            } else {
                main = separated[SizeCategory.Other][0];
            }

            return new ProductsRelation {
                ProductList = list,
                MainProduct = main,
                ProductsRelationParts = related.Select(product => new ProductsRelation.ProductsRelationPart {
                    Id = -1,
                    RelatedProduct = product,
                    CreatedBy = -1,
                    CreatedTimestamp = Utils.GetCurrentTimestamp()
                }).ToList()
            };
        }

        private static Dictionary<SizeCategory, List<Product>> SeparateBySizeCategories(List<Product> products) {
            var result = new Dictionary<SizeCategory, List<Product>> {
                {SizeCategory.XsToXxxl, new List<Product>()},
                {SizeCategory.Numeric, new List<Product>()},
                {SizeCategory.Other, new List<Product>()}
            };

            products.ForEach(product => {
                if (XsToXxxlSizesOrderingWeight.ContainsKey(product.Size.Name)) {
                    result[SizeCategory.XsToXxxl].Add(product);
                } else if (product.Size.IsNumeric) {
                    result[SizeCategory.Numeric].Add(product);
                } else {
                    result[SizeCategory.Other].Add(product);
                }
            });

            result[SizeCategory.XsToXxxl].Sort(
                (p1, p2) =>
                    XsToXxxlSizesOrderingWeight[p1.Size.Name].CompareTo(XsToXxxlSizesOrderingWeight[p2.Size.Name]));

            result[SizeCategory.Numeric].Sort((p1, p2) => p1.Size.Number.CompareTo(p2.Size.Number));

            return result;
        }

        private enum SizeCategory {
            XsToXxxl,
            Numeric,
            Other
        };
    }
}