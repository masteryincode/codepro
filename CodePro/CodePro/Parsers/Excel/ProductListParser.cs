﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using CodePro.POCO;
using CodePro.Properties;
using NLog;
using OfficeOpenXml;

namespace CodePro.Parsers.Excel {
    public class ProductListParser {
        private const string DefaultWorkSheetName = "BOLDetails1.xls";
        private const int ColumnsNumber = 13;
        private static readonly Logger Log = LogManager.GetLogger("ProductListParser");
        private readonly Action<int> _onNext;

        public ProductListParser(Action<int> onNext) {
            _onNext = onNext;
        }

        public List<ProductToProductListBuffer> Parse(string path, string workSheetName = DefaultWorkSheetName,
            bool hasHeader = true, bool hasFirstNumberColumn = true) {
            var sw = new Stopwatch();
            sw.Start();
            Log.Info(@"The excel file '{0}' parsing is started", path);

            var result = new List<ProductToProductListBuffer>();

            using (var excelPackage = new ExcelPackage(new FileInfo(path))) {
                var workSheet = excelPackage.Workbook.Worksheets[workSheetName] ?? excelPackage.Workbook.Worksheets[1];

                var startRow = hasHeader ? 2 : 1;
                var startColumn = hasFirstNumberColumn ? 2 : 1;
                var currentColumnsNumber = workSheet.Dimension.End.Column - startColumn + 1;

                Log.Debug("Current columns number: {0}", currentColumnsNumber);

                if (workSheet.Dimension.End.Row < startRow ||
                    currentColumnsNumber < ColumnsNumber - 1) {
                    sw.Stop();
                    Log.Error("Wrong columns number: {0}", currentColumnsNumber);

                    return result;
                }

                var timestamp = Utils.GetCurrentTimestamp();
                var progress = 0;

                _onNext(0);

                var rowsNumber = workSheet.Dimension.End.Row;

                Log.Debug("Rows Number: {0}", rowsNumber);

                for (var row = startRow; row <= rowsNumber; ++row) {
                    if (string.IsNullOrEmpty(workSheet.Cells[row, startColumn].Text)) {
                        break;
                    }

                    var buffer = new ProductToProductListBuffer {
                        Id = -1,
                        ProductListName = $"{path} {timestamp}",
                        Upc = long.Parse(workSheet.Cells[row, startColumn].Text),
                        Description = string.Copy(workSheet.Cells[row, startColumn + 1].Text).Trim(),
                        OriginalQuantity = int.Parse(workSheet.Cells[row, startColumn + 2].Text),
                        OriginalCost =
                            Utils.ParseCurrency(row, startColumn + 3,
                                workSheet.Cells[row, startColumn + 3].Value.ToString()),
                        OriginalRetail =
                            Utils.ParseCurrency(row, startColumn + 4,
                                workSheet.Cells[row, startColumn + 4].Value.ToString()),
                        VendorOrStyleNumber = string.Copy(workSheet.Cells[row, startColumn + 5].Text).Trim(),
                        Color = string.Copy(workSheet.Cells[row, startColumn + 6].Text).Trim(),
                        Size = string.Copy(workSheet.Cells[row, startColumn + 7].Text).Trim(),
                        ClientCost =
                            Utils.ParseCurrency(row, startColumn + 8,
                                workSheet.Cells[row, startColumn + 8].Value.ToString()),
                        Division = string.Copy(workSheet.Cells[row, startColumn + 9].Text).Trim(),
                        DepartmentName = string.Copy(workSheet.Cells[row, startColumn + 10].Text).Trim(),
                        VendorName = string.Copy(workSheet.Cells[row, startColumn + 11].Text).Trim(),
                        ImageUrl = string.Copy(workSheet.Cells[row, startColumn + 12].Text).Trim(),
                        Status = Resources.NOT_PROCESSED,
                        ProcessingResult = "",
                        CreatedTimestamp = Utils.GetCurrentTimestamp(),
                        CreatedBy = -1
                    };

                    result.Add(buffer);

                    var currentProgress =
                        (int) ((row - startRow + 1) * 100.0 / (workSheet.Dimension.End.Row - startRow + 1));

                    if (currentProgress <= progress) continue;

                    _onNext(currentProgress);
                    progress = currentProgress;
                }

                if (progress != 100) {
                    _onNext(100);
                }
            }

            sw.Stop();
            Log.Info(@"The excel file parsing is stopped. Elapsed={0} Parsed={1}", sw.Elapsed, result.Count);


            return result;
        }
    }
}