﻿using System.Configuration;
using MySql.Data.MySqlClient;

namespace CodePro.DB {
    public static class Utils {
        public static MySqlConnection GetConnection() {
            return
                new MySqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
        }
    }
}