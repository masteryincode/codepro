drop database if exists `codepro`;
create database if not exists `codepro`;
grant all privileges on `codepro` . * to 'codeprouser'@'%';