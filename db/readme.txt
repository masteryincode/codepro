1) modify (if it necessary) create_user_and_db.sql // change codepro login and password

2) run create_user_and_db.cmd [admin_login] [admin_password]
	or
   recreate_db.cmd [admin_login] [admin_password]

3) modify migrate.cmd // change codepro login and password
	and run

4) run rollback.cmd if step 3 was unsuccessful

